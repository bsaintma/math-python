# CONTRIBUTING GUIDE

- Type hinting is essential
- Every function must have a short comment explaining what it does, the parameters and the return value attended
- Each merge request must be reviewed before merging
- Respect PEP8 convention